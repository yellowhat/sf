#!/usr/bin/env python3
"""Benchmark using valgrind (cachegrind)"""

from os import environ
from pexpect import spawn


env = environ
env["TERM"] = "xterm-256color"


def run_case(keys: str, args: str = "") -> None:
    """Run sf with keys and args and print output"""

    outfile = "/tmp/cachegrind"
    opts = [
        "--tool=cachegrind",
        f"--I1={256 * 1024},8,64",
        f"--D1={256 * 1024},8,64",
        f"--LL={8 * 1024 ** 2},16,64",
        "--cache-sim=yes",
        "--branch-sim=yes",
        "--trace-children=yes",
        f"--cachegrind-out-file={outfile}",
        "python",
        "sf.py",
        args,
    ]

    proc = spawn("valgrind", opts, encoding="UTF8", env=env)
    proc.sendline(keys)
    out = proc.read()
    proc.close()
    print(out)

    with open(outfile, encoding="UTF8") as obj:
        for line in obj.readlines():
            if line.startswith("events: ") or line.startswith("summary: "):
                print(line)


run_case("q")
