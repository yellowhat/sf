#!/usr/bin/env python3
"""Show available colors"""

import curses

BACKGROUND = 0


def main(window) -> None:
    """Run curses commands"""

    curses.start_color()
    curses.use_default_colors()

    for i in range(curses.COLORS):
        curses.init_pair(i + 1, i, BACKGROUND)
    window.addstr(0, 0, f"{curses.COLORS} colors available")

    col, row = 0, 1
    for i in range(curses.COLORS):
        window.addstr(row, col, f"{i:5}", curses.color_pair(i))
        col += 5
        if (i + 1) % 16 == 0:
            col = 0
            row += 1
    window.getch()


curses.wrapper(main)
