#!/usr/bin/env bash
# Update version
set -euo pipefail

VERSION=$1

echo "[INFO] Current:"
grep "__version__" sf.py

sed -e "s|__version__ =.*|__version__ = \"$VERSION\"|" \
    -i sf.py

echo "[INFO] Replaced:"
grep "__version__" sf.py
