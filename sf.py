#!/usr/bin/env python3
"""A simple file manager written in python"""

import curses
from argparse import ArgumentParser
from curses.ascii import ESC
from functools import partial
from os import environ, makedirs
from math import log
from pathlib import Path
from shutil import which
from stat import filemode
from string import Template
from subprocess import run, PIPE
from tempfile import NamedTemporaryFile
from typing import Any, Collection, Dict, List, Optional, Set, TYPE_CHECKING

if TYPE_CHECKING:
    from _curses import window as Window
else:
    Window = Any

__version__ = "1.0.0"
EDITOR = environ.get("EDITOR", "vi")


def opener(path_obj: Path) -> str:
    """Return command to run based on file extension"""

    video = [".avi", ".flv", ".mkv", ".mp4", ".mpg", ".webm"]
    image = [".bmp", ".gif", ".jpg", ".jpeg", ".png", ".tif", ".tiff"]
    audio = [".m4a", ".mp3", ".ogg", ".opus", ".wav"]
    ext = path_obj.suffix.lower()
    path = '"' + path_obj.absolute().as_posix() + '"'

    if any(ext == e for e in video + image):
        return f"nohup flatpak run io.mpv.Mpv --force-window {path} &>/dev/null &"
    if any(ext == e for e in audio):
        return f"nohup flatpak run io.mpv.Mpv --force-window --no-video {path} &>/dev/null &"
    if any(ext == e for e in [".doc", ".ods", ".xls", ".xlsx"]):
        return f"nohup flatpak run org.libreoffice.LibreOffice {path} &>/dev/null &"
    if any(ext == e for e in [".epub", ".pdf"]):
        return f"flatpak run io.yellowhat.zathura {path} --fork &>/dev/null"
    return f"{EDITOR} {path}"


def fmt_size(size: int) -> str:
    """Format a bytes size in human format"""

    if size == 0:
        return f"{0:6d}  B"

    units = ["B", "kB", "MB", "GB", "TB", "PB"]
    decms = [0, 0, 1, 2, 2, 2]
    exponent = min(int(log(size, 1024)), len(units) - 1)
    quotient = float(size) / 1024**exponent
    unit, ndecm = units[exponent], decms[exponent]
    return f"{quotient:6.{ndecm}f} {unit:>2s}"


class Sf:
    """Sf class"""

    def __init__(
        self,
        dircur: str = ".",
        showhidden: bool = False,
        mark_file: str = "/tmp/.sf_marked",
    ):
        # Set the number of milliseconds to wait after reading an escape character
        curses.set_escdelay(10)

        # Padding
        self.padl = 1
        self.padt = 2

        # Check progress option for cp/mv
        out = run(
            "cp --help",
            shell=True,
            check=True,
            capture_output=True,
        ).stdout.decode("UTF-8")
        opt_g = "--progress-bar" if "--progress-bar" in out else ""

        # Keybindings
        self.keybindings: Dict[str, Any] = {
            "?": self.show_keybindings,
            **dict.fromkeys(["k", "KEY_UP"], partial(self.move_cursor, -1)),
            **dict.fromkeys(["j", "KEY_DOWN"], partial(self.move_cursor, +1)),
            "KEY_PPAGE": partial(self.move_cursor, -10),
            "KEY_NPAGE": partial(self.move_cursor, +10),
            "g": partial(self.move_cursor, -999999),
            "G": partial(self.move_cursor, +999999),
            **dict.fromkeys(["l", "KEY_RIGHT", "KEY_ENTER", "\n"], self.open),
            **dict.fromkeys(
                ["h", "KEY_LEFT", "KEY_BACKSPACE", "\b", "\x7f"], self.upper_dir
            ),
            ".": self.togglehidden,
            **dict.fromkeys(["p", " "], self.mark),
            "P": partial(self.mark, mark_all=True),
            "c": partial(self.mark_clean, refresh=True),
            "f": partial(self.create, "file"),
            "n": partial(self.create, "folder"),
            "r": self.rename,
            "y": partial(self.run_cmd, Template(f"cp -vir {opt_g} $files $dest")),
            "m": partial(self.run_cmd, Template(f"mv -vi {opt_g} $files $dest")),
            "x": partial(self.run_cmd, Template("rm -rf $files")),
            "KEY_RESIZE": self.show_dir,
            "e": self.read_dir,
            "!": self.open_shell,
            "/": self.search,
            "~": partial(self.go_dir, Path.home()),
            "1": partial(self.go_dir, Path.home() / "Downloads"),
            "2": partial(self.go_dir, Path("/media/Backup")),
        }
        if which("fzf") is not None:
            self.keybindings["d"] = self.fzf

        self.width = 0
        self.idxcur = 0
        self.dircur = Path(dircur if Path(dircur).is_dir() else ".").resolve()
        self.showhidden = showhidden
        self.items: List[Path] = []
        self.nitems = 0
        self.marked: Set[Path] = set()
        self.mark_file = mark_file

    def mainloop(self, window: Window) -> None:
        """Initialise curses, show and wait for key to be pressed"""

        self.window = window  # pylint: disable=attribute-defined-outside-init
        curses.start_color()
        curses.use_default_colors()
        curses.curs_set(False)

        # Header
        curses.init_pair(1, 7, 4)  # Header: White | Purple
        curses.init_pair(2, 7, 5)  # Header: White | Light Blue
        # Colors (Standard)
        curses.init_pair(11, 248, -1)  # Info: Grey | Transparent
        curses.init_pair(12, 6, -1)  # Folder (Sym): Blue | Transparent
        curses.init_pair(13, 12, -1)  # Folder: Blue | Transparent
        curses.init_pair(14, 10, -1)  # Exec: Green | Transparent
        curses.init_pair(15, 5, -1)  # Sym: Green | Transparent
        # Colors (Select)
        curses.init_pair(21, 1, -1)  # Info: Grey | Transparent

        self.mark_load()
        self.read_dir()
        while True:
            key = self.window.getkey()
            if key == "q":
                break
            self.keybindings.get(key, lambda: None)()

    def print_line(self, irow: int, path: Path) -> None:
        """Print an item of the self.items list"""

        stat = filemode(path.stat().st_mode)
        if path.is_dir():
            size = f"{sum(1 for _ in path.glob('*')):9d}"
        else:
            size = fmt_size(path.stat().st_size)
        self.window.addstr(
            irow + self.padt, self.padl, f"{stat} {size} ", curses.color_pair(11)
        )
        text = f"*{path.name}" if path in self.marked else f" {path.name}"
        if path.is_dir() and path.is_symlink():
            self.window.addnstr(text, self.width, curses.A_BOLD | curses.color_pair(12))
        elif path.is_dir():
            self.window.addnstr(text, self.width, curses.A_BOLD | curses.color_pair(13))
        elif which(path) is not None:
            self.window.addnstr(text, self.width, curses.color_pair(14))
        elif path.is_symlink():
            self.window.addnstr(text, self.width, curses.color_pair(15))
        else:
            self.window.addnstr(text, self.width)

    def show_dir(self, header: Optional[str] = None) -> None:
        """Show the content of the current folder"""

        self.window.clear()
        term_l, term_w = self.window.getmaxyx()
        maxitems = term_l - self.padt
        self.width = term_w - self.padl - 9 - 1 - 9 - 2 - 1
        if header is None:
            header = f"{self.idxcur + 1:4d}/{self.nitems} {self.dircur} "
        self.window.addnstr(0, self.padl, header, term_w - 1, curses.color_pair(1))
        if self.items:
            nshow = 3
            if maxitems - self.idxcur < nshow:
                idx_end = self.idxcur + nshow
                idx_start = idx_end - maxitems
            else:
                idx_start = 0
                idx_end = maxitems
            path_cur = self.items[self.idxcur]
            sep = 10 + 10 + 2
            for irow, path in enumerate(self.items[idx_start:idx_end]):
                self.print_line(irow, path)
                if path == path_cur:
                    self.window.move(irow + self.padt, self.padl)
                    self.window.chgat(sep, curses.color_pair(21))
                    self.window.move(irow + self.padt, self.padl + sep)
                    self.window.chgat(len(path.name), curses.A_REVERSE)
        else:
            self.window.addstr(self.padt, self.padl, "empty", curses.color_pair(12))
        self.window.refresh()

    def show_keybindings(self) -> None:
        """Show keybindings"""

        with NamedTemporaryFile(mode="w+") as temp_file:
            for key, value in self.keybindings.items():
                key = f"'{key}'"
                if hasattr(value, "func"):
                    doc = value.func.__doc__ + " "
                    for arg in value.args:
                        if hasattr(arg, "template"):
                            doc += arg.template
                        else:
                            doc += str(arg)
                else:
                    doc = value.__doc__
                temp_file.write(f"{key:4} | {doc}\n")
            temp_file.flush()
            curses.endwin()
            _ = run(f"{EDITOR} {temp_file.name}", shell=True, check=True)  # nosemgrep
            self.window.refresh()
        self.read_dir()

    def read_dir(self) -> None:
        """Read current folder content"""

        dirs = []
        files = []
        for path in Path(self.dircur).glob("*"):
            if (not path.name.startswith(".")) or self.showhidden:
                if path.is_dir():
                    dirs.append(path)
                else:
                    files.append(path)
        dirs.sort(key=lambda x: x.name.lower())
        files.sort(key=lambda x: x.stem.lower())
        self.idxcur = 0
        self.items = dirs + files
        self.nitems = len(self.items)
        self.show_dir()

    def move_cursor(self, nrows: int) -> None:
        """Move the cursors by nrows"""

        self.idxcur += nrows
        if self.idxcur < 0:
            self.idxcur = self.nitems - 1
        elif self.idxcur > self.nitems - 1:
            self.idxcur = 0
        self.show_dir()

    def go_dir(self, path: Path) -> None:
        """Go to folder"""

        self.dircur = path.resolve()
        if self.dircur.is_file():
            self.dircur = self.dircur.parent
        self.idxcur = 0
        self.read_dir()

    def upper_dir(self) -> None:
        """Go to the upper folder"""

        self.go_dir(self.dircur.parent)

    def open_shell(self) -> None:
        """Open a terminal in the current folder"""

        curses.endwin()
        _ = run(f"cd '{self.dircur}'; $0", shell=True, check=True)  # nosemgrep
        self.window.refresh()

    def open_app(self, path_obj: Path) -> None:
        """Open the current item in an external application"""

        curses.endwin()
        _ = run(opener(path_obj), shell=True, check=True)  # nosemgrep
        self.window.refresh()

    def open(self) -> None:
        """Open the current item"""

        if not self.items:
            return
        path = self.items[self.idxcur]
        if path.is_dir():
            self.go_dir(path.resolve())
        else:
            self.open_app(path)
            self.show_dir()

    def togglehidden(self) -> None:
        """Toggle to show/hide hidden items"""

        self.showhidden = not self.showhidden
        self.read_dir()

    def mark(self, mark_all: bool = False) -> None:
        """Mark an or all items and save to file the new set"""

        if mark_all:
            paths = self.items
        else:
            paths = [self.items[self.idxcur]]
        for path in paths:
            if path in self.marked:
                self.marked.remove(path)
            else:
                self.marked.add(path)
        with open(self.mark_file, "w", encoding="UTF8") as fobj:
            for path in self.marked:
                fobj.write(str(path) + "\n")
        self.show_dir()

    def mark_load(self) -> None:
        """Load marked items set from file"""

        if not Path(self.mark_file).is_file():
            self.mark_clean()
            return
        if Path(self.mark_file).stat().st_size == 0:
            self.mark_clean()
            return
        with open(self.mark_file, encoding="UTF8") as fobj:
            self.marked = {Path(s.strip()) for s in fobj.readlines()}

    def mark_clean(self, refresh: bool = False) -> None:
        """Empty marked items set"""

        self.marked = set()
        with open(self.mark_file, "w", encoding="UTF8"):
            pass
        if refresh:
            self.show_dir()

    def prompt(self, prompt: str) -> Optional[str]:
        """Prompt user for a string"""

        self.window.addstr(0, self.padl, prompt, curses.color_pair(2))
        self.window.clrtoeol()
        response = ""
        while True:
            key = self.window.getkey()
            if key == chr(ESC):
                return None
            if key in ["KEY_ENTER", "\n"]:
                return response
            if key in ["KEY_BACKSPACE", "\b", "\x7f"]:
                response = response[:-1]
            else:
                response += key
            self.window.addstr(
                0, self.padl + len(prompt), response, curses.color_pair(1)
            )
            self.window.clrtoeol()

    def prompt_yesno(self, header: str, extra: Collection[Path]) -> bool:
        """Prompt user for yes/no"""

        header += ", confirm with y?"
        curses.echo()
        self.window.clear()
        self.window.addstr(0, self.padl, header, curses.color_pair(1))
        i = 1
        for row in extra:
            self.window.addstr(i, self.padl, str(row), curses.color_pair(1))
            i += 1
        self.window.refresh()
        curses.noecho()
        key = self.window.getkey()
        return bool(key == "y")

    def create(self, typ: str) -> None:
        """Create a new empty file/folder if not existing"""

        name = self.prompt(f"New {typ} name: ")
        if name is None:
            self.show_dir()
            return
        path = Path(self.dircur) / Path(name)
        if not path.exists():
            if typ == "file":
                with open(path, "a", encoding="UTF8"):
                    pass
            elif typ == "folder":
                makedirs(path)
        self.read_dir()

    def rename(self) -> None:
        """Rename all items in current folder"""

        if not self.items:
            return
        with NamedTemporaryFile(mode="w+") as temp_file:
            for item in self.items:
                temp_file.write(item.name + "\n")
            temp_file.flush()
            curses.endwin()
            _ = run(f"{EDITOR} {temp_file.name}", shell=True, check=True)  # nosemgrep
            temp_file.seek(0)
            items_new = temp_file.read().splitlines()
            if len(items_new) == len(self.items):
                for src, dst in zip(self.items, items_new):
                    src.rename(src.parent / dst)
            self.window.refresh()
        self.read_dir()

    def run_cmd(self, template: Template) -> None:
        """Run a command in the shell (cp/mv/rm)"""

        self.mark_load()
        lst = self.marked if self.marked else [self.items[self.idxcur]]
        response = self.prompt_yesno(f"Run '{template.template.split()[0]}'", lst)
        curses.endwin()
        if response:
            files = " ".join(f'"{p.absolute()}"' for p in lst)
            cmd = template.substitute(files=files, dest=f"'{self.dircur}'")
            _ = run(cmd, shell=True, check=True)  # nosemgrep
            self.mark_clean()
        self.window.refresh()
        self.read_dir()

    def search(self) -> None:
        """Search in the current folder"""

        search = self.prompt("Search: ")
        if search is None:
            self.show_dir()
        else:
            self.items = [s for s in self.items if search in s.name]
            self.nitems = len(self.items)
            self.idxcur = 0
            header = f"Search: {search}"
            self.show_dir(header=header)

    def fzf(self) -> None:
        """Search a file using fzf and to to folder"""

        proc = run(  # nosemgrep
            f"find {self.dircur} | fzf",
            shell=True,  # nosemgrep
            check=False,
            stdout=PIPE,
            encoding="UTF-8",
        )
        self.go_dir(path=Path(proc.stdout.strip()))


if __name__ == "__main__":
    # Parse arguments
    parser = ArgumentParser(
        description="sf: a simple console file manager written in python"
    )
    parser.add_argument(
        "folder",
        default=".",
        nargs="?",
        help="Folder to run from",
    )
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version=__version__,
    )
    parser.add_argument(
        "--showhidden",
        default=False,
        action="store_true",
        help="show hidden files",
    )
    parser.add_argument(
        "--no-showhidden",
        dest="showhidden",
        action="store_false",
        help="hide hidden files (default)",
    )
    parser.add_argument(
        "--mark-file",
        default="/tmp/.sf_marked",
        help="mark file path",
    )
    args = parser.parse_args()

    # Mainloop
    sf = Sf(dircur=args.folder, showhidden=args.showhidden, mark_file=args.mark_file)
    try:
        curses.wrapper(sf.mainloop)
    except KeyboardInterrupt:
        pass
