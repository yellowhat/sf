#!/usr/bin/env python3
"""Test normal usage cases"""

import random
from pathlib import Path
from shutil import rmtree
from string import ascii_lowercase
from typing import Any, Tuple
from unittest import main, TestCase
from pexpect import spawn


def get_keys(action: str) -> list[str]:
    """Read keys from README.md"""

    translate = {
        "down": "\033OB",
        "up": "\033OA",
        "left": "\033OD",
        "right": "\033OC",
        "enter": "\n",
        "backspace": "\x7f",
        "escape": "\x1b",
        "space": " ",
    }
    with open("README.md", encoding="UTF8") as obj:
        lines = obj.readlines()
    keys = [line.split("|")[1].strip() for line in lines if action in line]
    return [translate.get(key, key) for key in keys]


def run_case(keys: str, args: str = "") -> Tuple[Any, Any]:
    """Run sf with keys and args"""

    proc = spawn(
        f"coverage run --parallel-mode sf.py {args}",
        encoding="UTF8",
        env={"TERM": "xterm-256color"},
    )
    proc.sendline(keys)
    out = proc.read()
    proc.close()

    return out, proc.exitstatus


def random_string(length: int) -> str:
    """Random string"""

    return "".join(random.choice(ascii_lowercase) for _ in range(length))  # nosec


class TestProcess(TestCase):
    """Test"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keys_q = get_keys("| quit")
        self.keys_h = get_keys("| go to parent folder")
        self.keys_l = get_keys("| go to child folder/open file")
        self.keys_k = get_keys("| scroll up")
        self.keys_j = get_keys("| scroll down")
        self.keys_f = get_keys("| new file")
        self.keys_n = get_keys("| new folder")
        self.keys_p = get_keys("| mark current item")
        self.keys_p_all = get_keys("| mark all items in current folder")
        self.keys_c = get_keys("| clear marked items list")
        self.keys_x = get_keys("| delete marked items")
        self.keys_term = get_keys("| open a terminal in the current folder")
        self.keys_srch = get_keys("| search in the current folder")
        self.keys_r = get_keys("| rename all items in current folder")
        self.keys_help = get_keys("| show keybindings")
        self.mark_file = "/tmp/.sf_marked"

    def test_00_open(self) -> None:
        """00. Open the current folder"""

        for key_q in self.keys_q:
            _, status = run_case(key_q)
            self.assertEqual(status, 0)

    def test_01_upper_dir(self) -> None:
        """01. Upper folder"""

        key_q = self.keys_q[0]
        for key_h in self.keys_h:
            for nkey in range(1, 5):
                _, status = run_case(f"{key_h * nkey}{key_q}")
                self.assertEqual(status, 0)
                _, status = run_case(f"{key_h * nkey}{key_q}", "/usr/bin")
                self.assertEqual(status, 0)

    def test_02_open_subfolder(self) -> None:
        """02. Open a subfolder in the current path"""

        key_q = self.keys_q[0]
        for fldr in ["a", "a a"]:
            # Create folders
            dir0 = Path(fldr)
            dir0.mkdir(parents=True)
            dir1 = Path(fldr * 2)
            dir1.symlink_to(dir0)
            # Empty (open directly)
            out, status = run_case(key_q, f"'{dir0}'")
            self.assertEqual(status, 0)
            self.assertEqual(len(out.splitlines()), 3)
            self.assertIn("empty", out)
            # Empty (move)
            for key_l in self.keys_l:
                out, status = run_case(f"{key_l}{key_q}")
                self.assertEqual(status, 0)
                page2 = out.split("\x1b[44m")[2].splitlines()
                self.assertEqual(len(page2), 2)
            # Create files under path
            for i in range(8):
                (dir0 / str(i)).write_text(f"Text {i}", encoding="UTF-8")
            (dir0 / str(i + 1)).symlink_to(str(i))
            with open(dir0 / "last", "w", encoding="UTF8"):
                pass
            # Non-empty (open directly)
            out, status = run_case(key_q, f"'{dir0}'")
            self.assertEqual(status, 0)
            self.assertEqual(len(out.splitlines()), 10)
            # Non-empty (move)
            for key_l in self.keys_l:
                out, status = run_case(f"{key_l}{key_q}")
                self.assertEqual(status, 0)
                page2 = out.split("\x1b[44m")[2].splitlines()
                self.assertEqual(len(page2), 9)
            # Hidden
            with open(dir0 / ".hid", "w", encoding="UTF8"):
                pass
            (dir0 / ".hidfolder").mkdir(parents=True)
            for key_l in self.keys_l:
                # Check not showing
                out, status = run_case(f"{key_l}{key_q}")
                self.assertEqual(status, 0)
                page2 = out.split("\x1b[44m")[2].splitlines()
                self.assertEqual(len(page2), 9)
                # Check showing
                out, status = run_case(f"{key_l}.{key_q}")
                self.assertEqual(status, 0)
                page2 = out.split("\x1b[44m")[3].splitlines()
                self.assertEqual(len(page2), 11)
            # Clean
            rmtree(dir0)
            Path(dir1).unlink()

    def test_03_showhidden(self) -> None:
        """03. Show/hide hidden files option"""

        key_q = self.keys_q[0]
        # Create folder
        fldr = "a"
        dir0 = Path(fldr)
        dir0.mkdir(parents=True)
        # Create files under path
        for i in range(2):
            (dir0 / f"file{i}").write_text(f"Text {i}", encoding="UTF-8")
        # Create hidden files under path
        for i in range(4):
            (dir0 / f".file{i}").write_text(f"Hidden {i}", encoding="UTF-8")
        # Default
        out, status = run_case(key_q, f"'{dir0}'")
        self.assertEqual(status, 0)
        self.assertEqual(len(out.splitlines()), 3)
        # --showhidden
        out, status = run_case(key_q, f"--showhidden '{dir0}'")
        self.assertEqual(status, 0)
        self.assertEqual(len(out.splitlines()), 7)
        # --no-showhidden
        out, status = run_case(key_q, f"--no-showhidden '{dir0}'")
        self.assertEqual(status, 0)
        self.assertEqual(len(out.splitlines()), 3)
        # Clean
        rmtree(dir0)

    def test_04_move_cursor(self) -> None:
        """04. Move cursor"""

        key_q = self.keys_q[0]
        for key in self.keys_k + self.keys_j:
            for nkey in range(1, 5):
                _, status = run_case(f"{key * nkey}{key_q}")
                self.assertEqual(status, 0)

    def test_04_move_cursor_long(self) -> None:
        """04. Move cursor on long names"""

        key_q = self.keys_q[0]
        # Create folder
        fldr = "a"
        dir0 = Path(fldr)
        dir0.mkdir(parents=True)
        # Create files
        for i in range(50):
            (dir0 / random_string(100)).write_text(f"Text {i}", encoding="UTF-8")
        # Create folders
        for i in range(50):
            (dir0 / random_string(100)).mkdir(parents=True)
        for key in self.keys_k + self.keys_j:
            for nkey in [100, 110]:
                _, status = run_case(f"{key * nkey}{key_q}", f"'{dir0}'")
                self.assertEqual(status, 0)
        # Clean
        rmtree(dir0)

    def test_05_create_file_folder(self) -> None:
        """05. Create file/folder"""

        key_q = self.keys_q[0]
        dir0 = Path("a")
        dir0.mkdir(parents=True)
        # File
        for key_f in self.keys_f:
            for name in ["f0", "f0 0"]:
                _, status = run_case(f"{key_f}{name}\n{key_q}", f"'{dir0}'")
                self.assertEqual(status, 0)
                self.assertEqual((dir0 / name).is_file(), True)
            # Backspace
            _, status = run_case(f"{key_f}ff0\x7f\n{key_q}", f"'{dir0}'")
            self.assertEqual(status, 0)
            self.assertEqual((dir0 / "ff").is_file(), True)
            # Escape
            _, status = run_case(f"{key_f}f000\x1b{key_q}", f"'{dir0}'")
            self.assertEqual(status, 0)
            self.assertEqual((dir0 / "f000").is_file(), False)
        # Folder
        for key_n in self.keys_n:
            for name in ["d0", "d0 0"]:
                _, status = run_case(f"{key_n}{name}\n{key_q}", f"'{dir0}'")
                self.assertEqual(status, 0)
                self.assertEqual((dir0 / name).is_dir(), True)
            # Backspace
            _, status = run_case(f"{key_n}dd0\x7f\n{key_q}", f"'{dir0}'")
            self.assertEqual(status, 0)
            self.assertEqual((dir0 / "dd").is_dir(), True)
            # Escape
            _, status = run_case(f"{key_n}d000\x1b{key_q}", f"'{dir0}'")
            self.assertEqual(status, 0)
            self.assertEqual((dir0 / "d000").is_dir(), False)
        # Clean
        rmtree(dir0)

    def test_06_scroll(self) -> None:
        """06. Scroll"""

        key_q = self.keys_q[0]
        key_f = self.keys_f[0]
        key_n = self.keys_n[0]
        key_l = self.keys_l[0]
        keys = f"{key_n}a\n{key_l}"
        keys += "".join(f"{key_f}a{i}\n" for i in range(50))
        for key in self.keys_k + self.keys_j:
            keys += f"{key * 100}"
        keys += key_q
        out, status = run_case(keys)
        self.assertEqual(status, 0)
        page = out.rsplit("\x1b[44m", maxsplit=1)[-1].splitlines()
        self.assertEqual(len(page), 22)
        # Clean
        rmtree("a")

    def test_07_mark(self) -> None:
        """07. Mark items"""

        key_q = self.keys_q[0]
        key_j = self.keys_j[0]
        dir0 = Path("a")
        dir0.mkdir(parents=True)
        p_file0 = dir0 / "a0"
        p_file0.write_text("Text 0")
        p_file1 = dir0 / "a1"
        p_file1.write_text("Text 1")
        for key_c in self.keys_c:
            for key_p in self.keys_p:
                # Mark
                _, status = run_case(f"\n{key_p}{key_q}")
                self.assertEqual(status, 0)
                with open(self.mark_file, encoding="UTF8") as obj:
                    path = obj.read()
                self.assertIn(str(p_file0), path)
                # Mark (double)
                _, status = run_case(f"\n{key_p}{key_q}")
                self.assertTrue(Path(self.mark_file).stat().st_size == 0)
                # Mark (2nd)
                _, status = run_case(f"\n{key_j}{key_p}{key_q}")
                self.assertEqual(status, 0)
                with open(self.mark_file, encoding="UTF8") as obj:
                    path = obj.read()
                self.assertIn(str(p_file1), path)
                # Mark clean
                _, status = run_case(f"\n{key_p}{key_c}{key_q}")
                self.assertTrue(Path(self.mark_file).stat().st_size == 0)
            for key_p_all in self.keys_p_all:
                # Mark all
                _, status = run_case(f"\n{key_p_all}{key_q}")
                self.assertEqual(status, 0)
                with open(self.mark_file, encoding="UTF8") as obj:
                    path = obj.read()
                self.assertIn(str(p_file0), path)
                self.assertIn(str(p_file1), path)
                # Mark clean
                _, status = run_case(f"\n{key_p_all}{key_q}")
                self.assertTrue(Path(self.mark_file).stat().st_size == 0)
        # Clean
        rmtree(dir0)

    def test_07_markfile(self) -> None:
        """07. Custom mark file"""

        key_q = self.keys_q[0]
        key_p = self.keys_p[0]
        dir0 = Path("a")
        dir0.mkdir(parents=True)
        p_file0 = dir0 / "a0"
        p_file0.write_text("Text 0")
        p_file1 = dir0 / "a1"
        p_file1.write_text("Text 1")
        # Mark
        mark_file = "/tmp/xxx"
        _, status = run_case(f"\n{key_p}{key_q}", f"--mark-file {mark_file}")
        self.assertEqual(status, 0)
        with open(mark_file, encoding="UTF8") as obj:
            path = obj.read()
        self.assertIn(str(p_file0), path)
        # Wrong path
        mark_file = "/tmp/xxx/xxx"
        _, status = run_case(f"\n{key_p}{key_q}", f"--mark-file {mark_file}")
        self.assertEqual(status, 1)
        # Clean
        rmtree(dir0)

    def test_08_remove_item(self) -> None:
        """08. Remove item"""

        key_q = self.keys_q[0]
        key_l = self.keys_l[0]
        for key_x in self.keys_x:
            dir0 = Path("a")
            dir0.mkdir(parents=True)
            p_file = dir0 / "f"
            p_file.write_text("Text", encoding="UTF-8")
            # Remove file
            _, status = run_case(f"{key_l}{key_x}y{key_q}")
            self.assertEqual(status, 0)
            self.assertEqual(p_file.is_file(), False)
            # Remove folder
            _, status = run_case(f"{key_x}y{key_q}")
            self.assertEqual(status, 0)
            self.assertEqual(dir0.is_file(), False)

    def test_09_open_shell(self) -> None:
        """09. Open Shell"""

        key_q = self.keys_q[0]
        fil0 = Path("zzz")
        for key in self.keys_term:
            _, status = run_case(f"{key}exit 0\n{key_q}")
            self.assertEqual(status, 0)
            _, status = run_case(f"{key}exit 11\n{key_q}")
            self.assertEqual(status, 1)
            _, status = run_case(f"{key}touch {fil0}\nexit 0\n{key_q}")
            self.assertEqual(status, 0)
            self.assertEqual(fil0.is_file(), True)
            fil0.unlink()

    def test_10_rename(self) -> None:
        """10. Rename"""

        key_q = self.keys_q[0]
        for key_r in self.keys_r:
            # Just open EDITOR
            _, status = run_case(f"{key_r}:wq\n{key_q}")
            self.assertEqual(status, 0)
            # Rename a file
            for name in ["a", "a-b", "a b", "a b c"]:
                dir0 = Path("a")
                dir0.mkdir(parents=True)
                fil0 = dir0 / name
                fil0.write_text("Text", encoding="UTF-8")
                _, status = run_case(
                    f"{key_r}:%s/{fil0.stem}/zz{name}zz/g\n:wq\n{key_q}", f"'{dir0}'"
                )
                self.assertEqual(status, 0)
                self.assertEqual(fil0.is_file(), False)
                self.assertEqual((dir0 / f"zz{name}zz").is_file(), True)
                # Clean
                rmtree(dir0)

    def test_11_open_app(self) -> None:
        """11. Open Application"""

        key_q = self.keys_q[0]
        key_k = self.keys_k[0]
        for key_l in self.keys_l:
            _, status = run_case(f"{key_k}{key_l}:q!\n{key_q}")
            self.assertEqual(status, 0)

    def test_12_search(self) -> None:
        """12. Search in the current folder"""

        key_q = self.keys_q[0]
        for key_srch in self.keys_srch:
            dir0 = Path("a")
            dir0.mkdir(parents=True)
            for i in range(5):
                (dir0 / f"a{i}").write_text(f"Text a {i}", encoding="UTF-8")
            for i in range(10):
                (dir0 / f"b{i}").write_text(f"Text b {i}", encoding="UTF-8")
            # a
            out, status = run_case(f"{key_srch}a\n{key_q}", f"'{dir0}'")
            self.assertEqual(status, 0)
            page = out.rsplit("\x1b[44m", maxsplit=1)[-1].splitlines()
            self.assertEqual(len(page), 5)
            # b
            out, status = run_case(f"{key_srch}b\n{key_q}", f"'{dir0}'")
            self.assertEqual(status, 0)
            page = out.rsplit("\x1b[44m", maxsplit=1)[-1].splitlines()
            self.assertEqual(len(page), 10)
            # Escape
            out, status = run_case(f"{key_srch}a\x1b{key_q}", f"'{dir0}'")
            self.assertEqual(status, 0)
            page = out.rsplit("\x1b[44m", maxsplit=1)[-1].splitlines()
            self.assertEqual(len(page), 15)
            # Backspace
            out, status = run_case(f"{key_srch}bb\x7f\n{key_q}", f"'{dir0}'")
            self.assertEqual(status, 0)
            page = out.rsplit("\x1b[44m", maxsplit=1)[-1].splitlines()
            self.assertEqual(len(page), 10)
            # Clean
            rmtree("a")

    def test_13_keybindings(self) -> None:
        """13. Show keybindings"""

        key_q = self.keys_q[0]
        for key_help in self.keys_help:
            _, status = run_case(f"{key_help}:q\n{key_q}")
            self.assertEqual(status, 0)


if __name__ == "__main__":
    main()
