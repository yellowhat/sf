#!/usr/bin/env python3
"""UnitTest opener function"""

from importlib.machinery import SourceFileLoader
from importlib.util import spec_from_loader, module_from_spec
from pathlib import Path
from unittest import main, TestCase

loader = SourceFileLoader(fullname="sf.py", path="sf.py")
spec = spec_from_loader(loader.name, loader)
sf = module_from_spec(spec)
loader.exec_module(sf)


class TestProcess(TestCase):
    """Test"""

    def test_audio(self) -> None:
        """Audio files"""

        exts = [".m4a", ".mp3", ".ogg", ".opus", ".wav"]
        for func in [str.lower, str.upper]:
            for ext in map(func, exts):
                out = sf.opener(Path(f"file{ext}"))
                self.assertIn("mpv", out)

    def test_image(self) -> None:
        """Image file"""

        exts = [".bmp", ".gif", ".jpg", ".jpeg", ".png", ".tif", ".tiff"]
        for func in [str.lower, str.upper]:
            for ext in map(func, exts):
                out = sf.opener(Path(f"file{ext}"))
                self.assertIn("mpv", out)

    def test_video(self) -> None:
        """Video files"""

        exts = [".avi", ".flv", ".mkv", ".mp4", ".mpg", ".webm"]
        for func in [str.lower, str.upper]:
            for ext in map(func, exts):
                out = sf.opener(Path(f"file{ext}"))
                self.assertIn("mpv", out)

    def test_docs(self) -> None:
        """Office documents files"""

        exts = [".doc", ".ods", ".xls", ".xlsx"]
        for func in [str.lower, str.upper]:
            for ext in map(func, exts):
                out = sf.opener(Path(f"file{ext}"))
                self.assertIn("libreoffice", out)

    def test_pdf(self) -> None:
        """pdf files"""

        exts = [".epub", ".pdf"]
        for func in [str.lower, str.upper]:
            for ext in map(func, exts):
                out = sf.opener(Path(f"file{ext}"))
                self.assertIn("zathura", out)


if __name__ == "__main__":
    main()
