#!/usr/bin/env bash
# Compare coverage
set -euo pipefail

# Get coverage for current pipeline
latest=$(curl -s "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}" | jq -r ".coverage")
latest="${latest/.*/}"
echo "[INFO] Pipeline $CI_PIPELINE_ID coverage value = $latest"

# Get coverage for last successful pipeline
tmp=$(curl -s "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines?status=success" | jq ".[0] | .id")
previous=$(curl -s "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${tmp}" | jq -r ".coverage")
echo "[INFO] previous coverage = $previous"
previous="${previous/.*/}"
echo "[INFO] previous coverage value = $previous"

# Check
if [ "$previous" == "null" ]; then
    echo "[INFO] Previous is null, skipping..."
    exit 0
elif [ "$latest" -ge "$previous" ]; then
    echo "[INFO] Latest pipeline coverage >= previous"
    exit 0
else
    echo "[ERROR] Latest pipeline coverage < previous"
    exit 1
fi
