#!/usr/bin/env python3
"""Test force cases"""

from time import sleep
from unittest import main, TestCase
from pexpect import spawn


class TestProcess(TestCase):
    """Test"""

    def test_0_force_close(self) -> None:
        """0. Force close"""

        proc = spawn(
            "coverage run --parallel-mode sf.py",
            encoding="UTF-8",
            env={"TERM": "xterm-256color"},
        )
        sleep(1)
        proc.sendcontrol("c")
        _ = proc.read()
        proc.close()
        self.assertEqual(proc.exitstatus, 0)


if __name__ == "__main__":
    main()
